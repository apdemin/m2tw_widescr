#include "stdafx.h"
#include "CPatch.h"

HANDLE HndThread;
DWORD jmpAddress;
DWORD width;
DWORD storage;
void patch_res();

float* aspectX_normalVal = (float *)0x16EA074;// 1024/config_val(1920)
float* aspectY_normalVal = (float *)0x16EA078;// 768/config_val(1080)

float* aspectXUiStratVal = (float *)0x169E0D8;// config_val(1920)/1024
float* aspectYUiStratVal = (float *)0x169E0DC;// config_val(1080)/768
float* aspect3d = (float *)0x2CE75DC; //if enabled widescreen in setting - 16/9 = 1.777777, else 4/3
int* width_conf = (int *)0x1647c34;//strat map conf width
short* height_conf = (short *)0x1647c38;//strat map conf height
//float* UIMenuWidth = (float *)0x2CB7108;  


int aspectX_normal = 0x16EA074;
int aspectY_normal = 0x16EA078;
int UIMenuWidth = 0x02CB7108;
int aspectXUiStrat = 0x169E0D8;
int aspectYUiStrat = 0x169E0DC;


void __declspec(naked)patch_menuUi()
{
	width = (short)*height_conf * 1024 / 768; //for fullscreen only. maybe need get real window size variables

	_asm
	{
			MOV storage,EAX

			MOV EAX,width
			MOV DWORD PTR DS : [0x2CB7108], EAX

			MOV EAX, storage
			mov jmpAddress, 0xC9EB82
			jmp jmpAddress
	}
}


int Thread()
{

	while ((int)*width_conf < 1024){
		Sleep(0);
	}
	CPatch::RedirectJump(0xC9EB7D, patch_menuUi);

	while (1)
	{
		if ((float)*aspectXUiStratVal == (float)*aspectYUiStratVal){
			Sleep(0);
		}
		else{
			CPatch::SetFloat(aspectX_normal, (float)*aspectY_normalVal);
			CPatch::SetFloat(aspectXUiStrat, (float)*aspectYUiStratVal);
		}
	}
	return 0;
}


BOOL APIENTRY DllMain(HMODULE hModule, DWORD reason, LPVOID lpReserved)
{
	if (reason == DLL_PROCESS_ATTACH)
	{
		HndThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)&Thread, NULL, 0, NULL);
	}
	return TRUE;
}