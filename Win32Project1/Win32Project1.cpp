#include "stdafx.h"
#include "CPatch.h"

HANDLE HndThread;

float* aspectX_normalVal = (float *)0x16EA074;// 1024/config_val(1920)
float* aspectY_normalVal = (float *)0x16EA078;// 768/config_val(1080)
	 
float* aspectXUiStratVal = (float *)0x169E0D8;// config_val(1920)/1024
float* aspectYUiStratVal = (float *)0x169E0DC;// config_val(1080)/768
float* aspect3d = (float *)0x2CE75DC; //if enabled widescreen in setting - 16/9 = 1.777777, else 4/3
//float* UIMenuWidth = (float *)0x2CB7108; //need valid formula/ 


int aspectX_normal = 0x16EA074;
int aspectY_normal = 0x16EA078;
int UIMenuWidth = 0x2CB7108;
int aspectXUiStrat = 0x169E0D8;
int aspectYUiStrat = 0x169E0DC;



int Thread()
{
	while (!(float)*aspect3d)
	{
		Sleep(0);
	}

	CPatch::SetFloat(aspectX_normal, (float)*aspectY_normalVal);
	CPatch::SetFloat(aspectXUiStrat, (float)*aspectYUiStratVal);
	CPatch::SetFloat(UIMenuWidth,1440.0);

	return 0;
}


BOOL APIENTRY DllMain(HMODULE hModule, DWORD reason, LPVOID lpReserved)
{
	if (reason == DLL_PROCESS_ATTACH)
	{
		HndThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)&Thread, NULL, 0, NULL);
	}
	return TRUE;
}